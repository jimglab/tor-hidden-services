## README

This is a coupling between [torsocks](https://github.com/dgoulet/torsocks/) and [Jekyll](https://jekyllrb.com).
The idea is to set up a webserver through Tor network using Jekyll to fix the web content.
So this repo organizes a container for Jekyll, and right after deploys a torsocks service with another container.


---
To up the Jekyll-Torsocks deployment of containers, use the [up.sh](./up.sh) script with these arguments:

* `-repo <ThisRepo>`: where `<ThisRepo>` is the path to this git repository.
* `-dirweb <path>`: to give the root path of the jekyll repository.
* `-dirlog <path>`: to give the path where runlogs will be saved. This directory will be mounted in the containers.

To stop the services, use the [down.sh](./down.sh) script.
<!--- TODO: arguments?? -->



---
### Set up the Jekyll part

First, pull a Jekyll docker image:
```bash
docker pull jekyll/jekyll:3.5
```

Now clone a jekyll theme. Here you can personalize the web contents (in a separate git repo).
The specific Jekyll tested here is:
```bash
git clone https://github.com/streetturtle/jekyll-clean-dark.git ./www
git reset --hard 3d8c1e071
```
<!--- TODO: mencionar los archivos clave para editar contenido. -->


<!--- EOF -->
