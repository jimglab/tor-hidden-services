#!/bin/bash

# "service" path
#ServicePath="/var/lib/tor/hidden_service"
ServicePath="/www"
sleep 20
mkdir -p $ServicePath
# NOTE: torsocks works this permisions
chmod 700 -R $ServicePath
# NOTE: torsocks needs this dir to be owned by root!
chown 0:0 -R $ServicePath
cd $ServicePath
#echo "greetings from rlab }:-)" > index.html

# local port where our service is running
# NOTE: this port number must be the same that uses our local
# web-server (be it a Python server, Apache, Jekyll or whatever!)
LocalPort=8080

torrc=/etc/tor/torrc
echo "HiddenServiceDir $ServicePath" >> $torrc
echo "HiddenServicePort 80 127.0.0.1:$LocalPort" >> $torrc

#--- runlgs
# set runlog files using as suffic the date/time of "now"
LogPython="/logs/python_webserver__`date +%d%b%Y_%Hh%Mm`.log"
LogTorsocks="/logs/torsocks__`date +%d%b%Y_%Hh%Mm`.log"
# report runlog paths inside the container
echo -e "\n [*] runlog for python webserver:\n $LogPython"
echo -e "\n [*] runlog for torsocks:\n $LogTorsocks"
echo ""

echo -e " [+] launch python server... "
cd $ServicePath
python2 -m SimpleHTTPServer 8080 > $LogPython 2>&1 &!

# launch onion route
echo -e " [+] launching torsocks as daemon ..."
# --Log debug/info/...
tor -f $torrc --Log info > $LogTorsocks 2>&1 &!
pid_tor=$!

# wait for torsocks to finish before exiting
# NOTE: if it fails, show the log :) so that the 
# container doesn't exit and we can troubleshoot.
wait ${pid_tor} \
    || tail -f $LogTorsocks

#EOF
