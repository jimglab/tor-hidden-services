#/bin/bash

#--- default values
ThisRepo="$PWD"
DirWeb="$PWD/www"
DirLogs="$PWD/logs"
Network="host"          # for the torsocks container

#--- cli-args
if [[ $# -eq 0 ]]; then
    echo -e "\n [-] $0 needs arguments!\n"
    exit 1
fi
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -repo)
        ThisRepo="$(realpath $2)"
        shift; shift;
        ;;
        -dirweb)
        DirWeb="$2"
        shift; shift;
        ;;
        -dirlog)
        DirLogs="$2"
        shift; shift;
        ;;
        -net) # TODO: allow torsocks container to go through a VPN
        Network="$2"
        shift; shift;
        ;;
        *)
        echo -e "\n [-] unknown argument: $1\n"
        exit 1
    esac
done

#--- checks
mkdir -p $DirLogs
[[ -d $ThisRepo && -d $DirWeb && -d $DirLogs ]] \
    || echo -e "\n [-] check input directories!\n"


#--- set env variables
# replace the string "<ThisRepo>" for "$DirRepo" given as argument above
ThisRepo_str=`echo $ThisRepo | sed "s/\\//\\\\\\\\\//g"`
sed -i "s/<ThisRepo>/${ThisRepo_str}/g" $ThisRepo/hidden-services/jekyll/.env
source $ThisRepo/hidden-services/jekyll/.env

# container names
CT_torsocks="hj_torsocks"
CT_contents="hj_contents"

# clean dangling
echo -e " [+] clean dangling containers ..."
docker rm ${CT_torsocks}
docker rm ${CT_contents} 


# NOTE: env variables for docker-compose are in ./env


#--- up
echo -e " [*] up the containers ..."
docker-compose up -d \
    && echo -e "\n [+] containers up!"
# grab the docker-compose process ID
#pid_compose=$!
echo -e "\n [*] waiting for torsocks... "
while ! docker exec -it -u 0:0 ${CT_torsocks} bash -c "ls /www/hostname > /dev/null 2>&1"; do 
    sleep 1; 
done

# show the onion URL
docker exec -it -u 0:0 ${CT_torsocks} \
    bash -c "echo -e '\n [*] ONION URL: ' && cat /www/hostname" \
    && echo -e "\n\n [+] all ok!\n"

echo -e " [*] showing stdout of torsocks container..."
docker logs -f ${CT_torsocks}



#EOF
