#!/bin/bash

CT_torsocks="hj_torsocks"
CT_contents="hj_contents"

#--- restore permisions
docker exec -it -u 0:0 ${CT_contents} \
    chown -R $(id -u):$(id -g) /srv/jekyll
docker exec -it -u 0:0 ${CT_contents} \
    chmod -R 744 $DirJekyll
# remove sensitive stuff
docker exec -it -u 0:0 ${CT_torsocks} \
    rm /www/hostname /www/private_key

# shutdown services
docker-compose down

# clean dangling
echo -e " [+] clean dangling containers ..."
docker rm ${CT_torsocks} 2> /dev/null
docker rm ${CT_contents} 2> /dev/null

# container names
source ./.env

#--- restore stuff
# restore owner, so we can modify contents before
# next restart
#sudo chown -R $(id -u):$(id -g) $DirJekyll
#sudo chmod -R 700 $DirJekyll

# TODO: we should test whether the onion address persists between
# restart of our torsocks container.
# TODO: move this to another location just right after the torsocks service goes up
# since "private_key" is sensitive info (impersonation stuff)
#rm $DirJekyll/_site/hostname $DirJekyll/_site/private_key


#EOF
