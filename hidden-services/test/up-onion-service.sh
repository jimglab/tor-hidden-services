#!/bin/bash

# "service" path
#ServicePath="/var/lib/tor/hidden_service"
ServicePath="/www"

mkdir -p $ServicePath
# NOTE: torsocks works this permisions
chmod 700 $ServicePath
# NOTE: torsocks needs this dir to be owned by root!
chown 0:0 $ServicePath
cd $ServicePath
echo "greetings from rlab }:-)" > index.html

# local port where our service is running
# NOTE: this port number must be the same that uses our local
# web-server (be it a Python server, Apache, Jekyll or whatever!)
LocalPort=8080

torrc=/etc/tor/torrc
echo "HiddenServiceDir $ServicePath" >> $torrc
echo "HiddenServicePort 80 127.0.0.1:$LocalPort" >> $torrc

echo " [+] launch python server... "
cd $ServicePath
python2 -m SimpleHTTPServer 8080 &!

# launch onion route
echo -e " [+] launching tor... "
tor -f $torrc --Log debug > /logs/torsocks.log 2>&1 &!
pid_tor=$!

# TODO: show the onion URL
echo -e " [*] ONION URL: "
cat /www/hostname

# wait for torsocks to finish before exiting
wait ${pid_tor}

#EOF
