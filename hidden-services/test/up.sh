#/bin/bash

# clean dangling
echo -e " [+] clean dangling containers ..."
docker rm torsocks
docker rm contents
mkdir -p ./logs ./www

# NOTE: env variables for docker-compose are in ./env


#--- up
echo -e " [+] up the containers ..."
docker-compose up -d
# grab the docker-compose process ID
pid_compose=$!
sleep 5
# show the onion URL
docker exec -it -u 0:0 torsocks \
    bash -c "echo -e '\n [*] ONION URL: ' && cat /www/hostname"
## wait for the deployment to finish before the next intructions ...
#wait ${pid_compose}
while [[ 1 ]]; do
    # TODO: abstraer esto porq otro container q contenga
    # el string "torsocks" en su nombre tmb puede matchear
    docker ps | grep torsocks > /dev/null 2>&1
    if [[ $? -ne 0 ]]; then break; fi
    sleep 1;
done


#--- restore stuff
# restore owner, so we can modify contents before
# next restart
sudo chown $(id -u):$(id -g) ./www
sudo chmod -R 700 ./www
# clean contents (??)
# TODO: we should test whether the onion address persists between
# restart of our torsocks container.
rm ./www/hostname ./www/private_key

#EOF
